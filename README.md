```bash
sudo apt-get -y install ansible git
git clone -b $(lsb_release -sc) https://gitlab.com/morph027/workstation-bootstrap.git /tmp/workstation-bootstrap
sudo ansible-playbook /tmp/workstation-bootstrap/system.yml

# or for non-morph027 users ;)
# sudo ansible-playbook /tmp/workstation-bootstrap/system.yml --skip-tags user
```
